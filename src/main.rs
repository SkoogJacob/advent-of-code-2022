use http_body_util::{BodyExt, Empty, StreamBody};
use hyper::body::Bytes;
use hyper::client::conn;
use hyper::Request;
use std::fmt::Write;
use hyper::http::uri::InvalidUri;
use tokio::io;
use tokio::io::{stdout, AsyncWriteExt as _};
use tokio::net::TcpStream;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let response_string = get_input_data("https://adventofcode.com/2022/day/1/input").await?;
    println!("{response_string}");
    Ok(())
}

/// Meant to read data from the input page for Advent of Code 2022 day 1
async fn get_input_data(target: &str) -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    // Get full address with host and port
    let url = target.parse::<hyper::Uri>().expect("Invalid URI");
    let address = get_host_port(&url);


    // Open TcpStream
    let stream = TcpStream::connect(address).await?;

    // Perform Tcp Handshake
    let (mut sender, conn) = conn::http1::handshake(stream).await?;

    // Check connection did not fail
    tokio::task::spawn(async move {
        if let Err(err) = conn.await {
            println!("Connection failed: {err:?}")
        }
    });

    let authority = url.authority().unwrap().clone();
    let req = Request::builder()
        .uri(url)
        .header(hyper::header::HOST, authority.as_str())
        .header(hyper::header::ACCEPT, "text/plain")
        .body(Empty::<Bytes>::new())?;
    let mut res = sender.send_request(req).await?;
    println!("Response status: {}", res.status());
    let mut response_string = String::with_capacity(8196);
    while let Some(next) = res.frame().await {
        let frame = next?;
        if let Some(chunk) = frame.data_ref() {
            let s = String::from_utf8_lossy(&chunk);
            response_string.push_str(&s);
        }
    }
    Ok(response_string)
}

fn get_host_port(url: &hyper::Uri) -> String {
    let host = url.host().expect("No host in URI");
    let port = url.port_u16().unwrap_or(80);
    format!("{host}:{port}")
}
